import cv2
import numpy as np
import matplotlib.pyplot as plt


def process_and_visualize(image_path):
    img = cv2.imread(image_path)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    gray_blurred = cv2.GaussianBlur(gray, (5, 5), 0)
    _, thresh_gaussian = cv2.threshold(gray_blurred, 100, 255, cv2.THRESH_BINARY)

    height, width, _ = img.shape

    height_limit = int(0.1 * height)

    for i in range(0, 22):
        x_position = int(i * 0.1 * width)
        if x_position >= width:
            x_position = width - 1

        cv2.line(img, (x_position, 0), (x_position, height_limit), (0, 255, 0), 2)

        percentage_text = f'{width / 10}'
        text_x_position = x_position + 5 if x_position + 50 < width else x_position - 100
        cv2.putText(img, percentage_text, (text_x_position, int(height_limit / 2)), cv2.FONT_HERSHEY_SIMPLEX, 2,
                    (255, 255, 255), 1)

    contours, _ = cv2.findContours(thresh_gaussian, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    for contour in contours:
        area = cv2.contourArea(contour)
        if area > 40:
            perimeter = cv2.arcLength(contour, True)
            approx = cv2.approxPolyDP(contour, 0.02 * perimeter, True)

            if len(approx) == 4:
                square_size_pixels = area
                square_size_mm = 10
                x, y, w, h = cv2.boundingRect(contour)
                reference_width_mm = w / square_size_mm

                cv2.drawContours(img, [approx], -1, (0, 255, 0), 2)

                cv2.putText(img, f'Width Pixel Size: {w:.2f} px', (x, y - 70), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0),2)
                break

    param_variations = [
        {'param1': 40, 'param2': 10, 'minRadius': 0, 'maxRadius': 60},
    ]

    num_variations = len(param_variations)
    fig, axs = plt.subplots(num_variations, 1, figsize=(50, 50), squeeze=False)
    axs = axs.flatten()

    for idx, variation in enumerate(param_variations):
        circles = cv2.HoughCircles(thresh_gaussian, cv2.HOUGH_GRADIENT, dp=0.1, minDist=30,
                                   param1=variation['param1'], param2=variation['param2'],
                                   minRadius=variation['minRadius'], maxRadius=variation['maxRadius'])

        img_copy = img.copy()
        if circles is not None:
            circles = np.uint16(np.around(circles))
            for i in circles[0, :]:
                diameter_px = 2 * i[2]
                cv2.circle(img_copy, (i[0], i[1]), i[2], (0, 255, 0), 2)
                cv2.circle(img_copy, (i[0], i[1]), 2, (0, 255, 0), 3)
                #diameter_text = f"{diameter_px}px"
                diameter_text = f"{diameter_px}"

                #diameter_text = f"{diameter_px / reference_width_mm:.2f}mm"

                cv2.putText(img_copy, diameter_text, (i[0] - 50, i[1]), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)

        axs[idx].imshow(cv2.cvtColor(img_copy, cv2.COLOR_BGR2RGB))
        axs[idx].axis('off')
        axs[idx].set_title(f"Image with param1={variation['param1']}, param2={variation['param2']}")



    diameters_mm = []

    for idx, variation in enumerate(param_variations):
        circles = cv2.HoughCircles(thresh_gaussian, cv2.HOUGH_GRADIENT, dp=0.1, minDist=30,
                                   param1=variation['param1'], param2=variation['param2'],
                                   minRadius=variation['minRadius'], maxRadius=variation['maxRadius'])

        if circles is not None:
            circles = np.uint16(np.around(circles))
            for i in circles[0, :]:
                diameter_px = 2 * i[2]
               # diameter_mm = diameter_px / reference_width_mm
                diameters_mm.append(diameter_px)
    return img_copy, diameters_mm


