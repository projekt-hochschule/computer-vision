import cv2
import numpy as np
from skimage.segmentation import clear_border


def process_and_visualize(image_path):
    image = cv2.imread(image_path)

    img_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    _, thresh = cv2.threshold(img_gray, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

    kernel = np.ones((3, 3), np.uint8)
    opening = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel, iterations=1)

    opening = clear_border(opening)

    sure_bg = cv2.dilate(opening, kernel, iterations=1)
    dist_transform = cv2.distanceTransform(opening, cv2.DIST_L2, 5)
    _, sure_fg = cv2.threshold(dist_transform, 0.1 * dist_transform.max(), 255, 0)
    sure_fg = np.uint8(sure_fg)
    unknown = cv2.subtract(sure_bg, sure_fg)
    _, markers = cv2.connectedComponents(sure_fg)
    markers = markers + 10
    markers[unknown == 255] = 0

    markers = cv2.watershed(image, markers)
    image[markers == -1] = [255, 255, 0]

    contours = []
    for label in np.unique(markers):
        if label != 0:
            mask = np.uint8(markers == label)
            contour, _ = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
            contours.append(contour[0])

    object_diameters_pixels = []

    for contour in contours:
        area = cv2.contourArea(contour)
        if area > 100:
            perimeter = cv2.arcLength(contour, True)
            approx = cv2.approxPolyDP(contour, 0.02 * perimeter, True)

            if len(approx) == 4:
                x, y, w, h = cv2.boundingRect(contour)
                reference_height_pixels = h
                reference_width_pixels = w

                cv2.drawContours(image, [approx], -1, (0, 255, 0), 2)

                cv2.putText(image, f'Height: {reference_height_pixels} pixels',
                            (int(approx[0][0][0]) - 50, int(approx[0][0][1]) - 30), cv2.FONT_HERSHEY_SIMPLEX, 1,
                            (0, 255, 0), 2)
                cv2.putText(image, f'Width: {reference_width_pixels} pixels',
                            (int(approx[0][0][0]) - 50, int(approx[0][0][1]) - 70), cv2.FONT_HERSHEY_SIMPLEX, 1,
                            (0, 255, 0), 2)

            if len(approx) != 4:
                (x, y), circle_radius_pixels = cv2.minEnclosingCircle(contour)
                circle_radius_pixels = int(circle_radius_pixels)

                object_diameter_pixels = circle_radius_pixels * 2
                object_diameters_pixels.append(object_diameter_pixels)

                cv2.circle(image, (int(x), int(y)), circle_radius_pixels, (0, 255, 0), 2)

                cv2.putText(image, f'{object_diameter_pixels}',
                            (int(x) - 40, int(y) + circle_radius_pixels + 30),
                            cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)

    return image, object_diameters_pixels

