import threading
import tkinter as tk
from tkinter import ttk, messagebox
from PIL import Image, ImageTk
import cv2
import os
import datetime

import h_t_image_processing_mm
import h_t_image_processing_pixel
import watershed_image_processing_mm
import watershed_image_processing_pixel
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg

class ImageApp:

    global relative_image_directory
    relative_image_directory = "Images/Lab"

    def __init__(self, root):
        self.root = root
        self.root.title("Image Viewer")

        # Set custom styles
        self.style = ttk.Style()
        self.style.theme_use("clam")  # Use a predefined theme for a modern look
        self.style.configure("TButton", font=("Arial", 10))
        self.style.configure("TLabel", font=("Arial", 10))

        # Main frame
        main_frame = ttk.Frame(root, padding=10)
        main_frame.grid(row=0, column=0, sticky="nsew")

        # Configure rows and columns to expand
        root.grid_rowconfigure(0, weight=1)
        root.grid_columnconfigure(0, weight=1)
        main_frame.grid_rowconfigure(0, weight=1)
        main_frame.grid_columnconfigure(0, weight=1)

        # Left frame
        left_frame = ttk.Frame(main_frame)
        left_frame.grid(row=0, column=0, padx=10, pady=10, sticky="nsew")

        # Toolbar
        toolbar = ttk.Frame(left_frame)
        toolbar.grid(row=0, column=0, padx=5, pady=5, sticky="ew")
        ttk.Button(toolbar, text="About", command=self.show_about).pack(side=tk.LEFT)

        # Left side scrollbar list
        self.image_list = tk.Listbox(left_frame, selectmode=tk.SINGLE)
        self.image_list.grid(row=1, column=0, padx=5, pady=5, sticky="nsew")
        #self.image_list.bind('<<ListboxSelect>>', self.start_processing())  # Bind the event
        self.image_list.bind('<<ListboxSelect>>', lambda event: self.show_selected_option(self.dropdown_var.get()))

        left_scroll = ttk.Scrollbar(left_frame, orient="vertical", command=self.image_list.yview)
        left_scroll.grid(row=1, column=1, sticky="ns")
        self.image_list.config(yscrollcommand=left_scroll.set)

        # Buttons frame
        buttons_frame = ttk.Frame(left_frame)
        buttons_frame.grid(row=2, column=0, padx=5, pady=5, sticky="ew")
        ttk.Button(buttons_frame, text="Take Image", command=self.take_image).pack(side=tk.LEFT, padx=5, pady=5, fill=tk.X, expand=True)
        ttk.Button(buttons_frame, text="Delete Image", command=self.delete_image).pack(side=tk.LEFT, padx=5, pady=5, fill=tk.X, expand=True)

        # Webcam live stream
        self.video_viewer = tk.Label(left_frame)
        self.video_viewer.grid(row=3, column=0, padx=5, pady=5, sticky="nsew")
        self.capture_video()



        self.info = tk.Label(left_frame)
        self.info.grid(row=4, column=0, padx=5, pady=5, sticky="nsew")
        self.info.config(text="1. select an image", font=("Arial", 15), foreground="white", background="#1D5B79")

        self.info = tk.Label(left_frame)
        self.info.grid(row=5, column=0, padx=5, pady=5, sticky="nsew")
        self.info.config(text="2. select a model (Watershed / Hough Transformation)", font=("Arial", 15),
                         foreground="white", background="#1D5B79")

        self.info = tk.Label(left_frame)
        self.info.grid(row=6, column=0, padx=5, pady=5, sticky="nsew")
        self.info.config(text="3. select the measurement (mm / pixel)", font=("Arial", 15), foreground="white", background="#1D5B79")

        # Right frame
        right_frame = ttk.Frame(main_frame)
        right_frame.grid(row=0, column=1, padx=10, pady=10, sticky="nsew")

        # Placeholder for histogram image
        self.histogram_frame_mm = tk.Frame(right_frame)
        self.histogram_frame_mm.grid(row=1, column=0, padx=5, pady=5, sticky="nsew")

        self.histogram_frame_pixels = tk.Frame(right_frame)
        self.histogram_frame_pixels.grid(row=2, column=0, padx=5, pady=5, sticky="nsew")

        # Placeholder for image with measured objects
        self.objects_label_mm = tk.Label(right_frame)
        self.objects_label_mm.grid(row=3, column=0, padx=5, pady=5, sticky="nsew")

        self.objects_label_pixels = tk.Label(right_frame)
        self.objects_label_pixels.grid(row=4, column=0, padx=5, pady=5, sticky="nsew")


        # Dropdown menu
        self.dropdown_var = tk.StringVar()
        self.dropdown_var.set("Select a Model")
        self.dropdown_menu = ttk.OptionMenu(right_frame, self.dropdown_var, "Select a Model", "Watershed",
                                            "Hough Transformation", command=self.show_selected_option)
        self.dropdown_menu.grid(row=0, column=0, padx=5, pady=5, sticky="nsew")
        self.style.configure("TMenubutton", foreground="#ded116", background="#1D5B79")  # Change to your desired colors

        # Hide all frames initially
        self.histogram_frame_mm.grid_remove()
        self.histogram_frame_pixels.grid_remove()
        self.objects_label_mm.grid_remove()
        self.objects_label_pixels.grid_remove()

        # Dropdown menu
        self.dropdown_var_ = tk.StringVar()
        self.dropdown_var_.set("Select a Measurement")
        self.dropdown_menu = ttk.OptionMenu(right_frame, self.dropdown_var_, "Select a Measurement", "Size in mm",
                                            "Size in pixels", command=self.show_selected_option)
        self.dropdown_menu.grid(row=0, column=1, padx=5, pady=5, sticky="nsew")
        self.style.configure("TMenubutton", foreground="#ded116", background="#1D5B79")  # Change to your desired colors

        # Populate image list initially
        self.populate_image_list()

        # Modify colors for buttons
        self.style.configure("TButton", foreground="black", background="#ded116")

        # Modify colors for labels
       # self.style.configure("TLabel", foreground="green", background="0f112b")
        self.style.configure("TLabel", foreground="white",background="#1D5B79")  


        # Modify colors for listbox
        self.style.configure("TListbox", foreground="black", background="0f112b")

        # Modify colors for frames
        self.style.configure("TFrame", foreground="black", background="#1D5B79")


    def populate_image_list(self):
        # Clear the current list
        global desktop_directory
        self.image_list.delete(0, tk.END)
        # Specify the directory where your images are located
        #image_directory = "/media/mo_09/Study/SoSe24/SW-Project/Abgabe_Final/Images"

        # Get the user's home directory
        home_directory = os.path.expanduser("~")

        # Determine the desktop directory based on the operating system
        if os.name == 'posix':  # Linux
            desktop_directory = os.path.join(home_directory, 'Desktop')
        elif os.name == 'nt':  # Windows
            desktop_directory = os.path.join(home_directory, 'Desktop')

        # Construct the full image directory path
        image_directory = os.path.join(desktop_directory, relative_image_directory)
        # Check if the directory exists
        if not os.path.exists(image_directory):
            # If the directory does not exist, create it
            os.makedirs(image_directory)
            print("Image directory created successfully.")
        else:
            print("Image directory already exists.")

        # Get image file paths in the specified directory
        image_files = [file for file in os.listdir(image_directory) if file.endswith(('jpg', 'jpeg', 'png'))]
        for img_name in image_files:
            img_path = os.path.join(image_directory, img_name)
            self.image_list.insert(tk.END, img_path)

    def capture_video(self):
       
        #Windows
        #self.cap = cv2.VideoCapture(1)

        #Linux
        self.cap = cv2.VideoCapture(4)

        self.show_frame()

    def show_frame(self):
        ret, frame = self.cap.read()
        if ret:
            # Display the live video stream
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            frame = Image.fromarray(frame)
            frame = ImageTk.PhotoImage(image=frame)
            self.video_viewer.configure(image=frame)
            self.video_viewer.image = frame  # Keep a reference to prevent garbage collection
            self.root.after(10, self.show_frame)


    def take_image(self):
        ret, frame = self.cap.read()
        if ret:
            # Resize frame for saving
            resized_frame = cv2.resize(frame, (3264, 1840))
            #resized_frame = cv2.resize(frame, (1840, 3264))

            
            # Specify the directory where images will be saved
            home_directory = os.path.expanduser("~")
            if os.name == 'posix':  # Linux
                desktop_directory = os.path.join(home_directory, 'Desktop')
            elif os.name == 'nt':  # Windows
                desktop_directory = os.path.join(home_directory, 'Desktop')

           # relative_image_directory = "Images/Lab"
            image_directory = os.path.join(desktop_directory, relative_image_directory)

            if not os.path.exists(image_directory):
                os.makedirs(image_directory)
                print("Image directory created successfully.")
            else:
                print("Image directory already exists.")

            current_time = datetime.datetime.now()
            img_name = current_time.strftime("img_%Y%m%d_%H%M%S.jpg")
            img_path = os.path.join(image_directory, img_name)

            # Convert BGR to RGB
            frame_rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            # Convert to PIL Image
            pil_image = Image.fromarray(frame_rgb)
            # Save the resized frame
            resized_pil_image = Image.fromarray(cv2.cvtColor(resized_frame, cv2.COLOR_BGR2RGB))
            resized_pil_image.save(img_path)
            # Update image list
            self.image_list.insert(tk.END, img_path)
            # Display the taken image
                
    def delete_image(self):
        # Get the selected image name
        selected_index = self.image_list.curselection()
        if selected_index:
            img_name = self.image_list.get(selected_index)
            # Remove the image file
            os.remove(img_name)
            # Update image list
            self.populate_image_list()
            messagebox.showinfo("Success", "Image deleted successfully.")

    def show_about(self):
        about_text = "This application.\nDeveloped by \n[Mahmoud Helmy] \nYamsi \nMuammer"
        messagebox.showinfo("About", about_text)

 
    def start_processing(self):
        selected_index = self.image_list.curselection()
        if selected_index:
            img_name = self.image_list.get(selected_index)
            processing_thread = threading.Thread(target=self.process_image, args=(img_name,))
            processing_thread.start()

    def process_image(self, img_name):
        option = self.dropdown_var.get()
        option_ = self.dropdown_var_.get()

        if option == "Watershed" and option_ == "Size in mm":
            image_1, object_diameters_mm = watershed_image_processing_mm.process_and_visualize(img_name)
            self.root.after(0, self.plot_histogram_mm, object_diameters_mm)
            self.root.after(0, self.update_objects_placeholder_mm, image_1)

        elif option == "Watershed" and option_ == "Size in pixels":
            image_2, object_diameters_pixels = watershed_image_processing_pixel.process_and_visualize(img_name)
            self.root.after(0, self.plot_histogram_pixels, object_diameters_pixels)
            self.root.after(0, self.update_objects_placeholder_pixels, image_2)

        elif option == "Hough Transformation" and option_ == "Size in mm":
            image_3, _object_diameters_mm = h_t_image_processing_mm.process_and_visualize(img_name)
            self.root.after(0, self.plot_histogram_mm, _object_diameters_mm)
            self.root.after(0, self.update_objects_placeholder_mm, image_3)

        elif option == "Hough Transformation" and option_ == "Size in pixels":
            image_4, _object_diameters_px = h_t_image_processing_pixel.process_and_visualize(img_name)
            self.root.after(0, self.plot_histogram_pixels, _object_diameters_px)
            self.root.after(0, self.update_objects_placeholder_pixels, image_4)

    def plot_histogram_mm(self, object_diameters_mm):
        hist, bins = np.histogram(object_diameters_mm, bins=30)
        fig, ax = plt.subplots()
        ax.bar(bins[:-1], hist, width=np.diff(bins), color='green', alpha=0.7)
        ax.set_title('Object Size Histogram (mm)')
        ax.set_xlabel('Size (mm)')
        ax.set_ylabel('Frequency')
        # Annotate each bar with its value if the height of the bar is greater than 0
        for i in range(len(bins) - 1):
            if hist[i] > 0:
                plt.text(bins[i], hist[i], f'{bins[i]:.1f}', ha='center', va='bottom', color='red', fontsize=8,
                         rotation=45)

        # Embedding the histogram plot into the GUI
        canvas = FigureCanvasTkAgg(fig, master=self.histogram_frame_mm)
        canvas.draw()
        canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=True)

    def plot_histogram_pixels(self, object_diameters_pixels):
        hist, bins = np.histogram(object_diameters_pixels, bins=30)
        fig, ax = plt.subplots()
        ax.bar(bins[:-1], hist, width=np.diff(bins), color='blue', alpha=0.7)
        ax.set_title('Object Size Histogram (pixels)')
        ax.set_xlabel('Size (pixels)')
        ax.set_ylabel('Frequency')

        # Annotate each bar with its value if the height of the bar is greater than 0
        for i in range(len(bins) - 1):
            if hist[i] > 0:
                plt.text(bins[i], hist[i], f'{bins[i]:.1f}', ha='center', va='bottom', color='red', fontsize=8,
                         rotation=45)

        # Embedding the histogram plot into the GUI
        canvas = FigureCanvasTkAgg(fig, master=self.histogram_frame_pixels)
        canvas.draw()
        canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=True)

    def update_objects_placeholder_mm(self, image):
        # Displaying the image with measured objects in millimeters...
        # Convert the OpenCV image to PIL format
        image_pil = Image.fromarray(cv2.cvtColor(image, cv2.COLOR_BGR2RGB))

        # Resize the image if needed
        image_pil = image_pil.resize((900, 600))

        # Convert the PIL image to Tkinter PhotoImage
        image_tk = ImageTk.PhotoImage(image_pil)

        # Update the placeholder label with the new image
        self.objects_label_mm.config(image=image_tk)
        self.objects_label_mm.image = image_tk

    def update_objects_placeholder_pixels(self, image):
        # Displaying the image with measured objects in pixels...
        # Convert the OpenCV image to PIL format
        image_pil = Image.fromarray(cv2.cvtColor(image, cv2.COLOR_BGR2RGB))

        # Resize the image if needed
        image_pil = image_pil.resize((900, 600))

        # Convert the PIL image to Tkinter PhotoImage
        image_tk = ImageTk.PhotoImage(image_pil)

        # Update the placeholder label with the new image
        self.objects_label_pixels.config(image=image_tk)
        self.objects_label_pixels.image = image_tk

    def show_selected_option(self, option):
        # Hide all frames initially
        self.histogram_frame_mm.grid_remove()
        self.histogram_frame_pixels.grid_remove()
        self.objects_label_mm.grid_remove()
        self.objects_label_pixels.grid_remove()

        if option in ["Size in mm", "Watershed", "Hough Transformation"]:
            # Show frames relevant to Item 1
            for widget in self.histogram_frame_mm.winfo_children():
                widget.destroy()
            self.histogram_frame_mm.grid(row=1, column=0, padx=5, pady=5, sticky="nsew")
            self.objects_label_mm.grid(row=2, column=0, padx=5, pady=5, sticky="nsew")

        #elif option == "Size in pixels" or option == "Watershed" or option == "Hough Transformation":
        elif option in ["Size in pixels", "Watershed", "Hough Transformation"]:

            # Show frames relevant to Item 2
            for widget in self.histogram_frame_pixels.winfo_children():
                widget.destroy()
            self.histogram_frame_pixels.grid(row=1, column=0, padx=5, pady=5, sticky="nsew")
            self.objects_label_pixels.grid(row=2, column=0, padx=5, pady=5, sticky="nsew")

        # Trigger processing based on the selected option
        self.start_processing()


if __name__ == "__main__":
    root = tk.Tk()
    app = ImageApp(root)
    root.mainloop()

